Kane, BORDERS
Vincent, MERMET-BIJON

## PART 1: DETERMINE PROBABILITIES

**Preliminary information**: We have a dataset that contains five years of data 
on what customers bought at what time of the day in a certain coffee bar. We 
know that the coffee bar is open every day from 8:00 to 18:00 and that the entire
day is split into fixed timeslots where only one customer buys something. We are
given that the customer will always buy one drink and sometimes one food item.

We begin by importing our data and packages (in Customer_Classes_init.py script) that will be used:

```python
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
from Code.Customer_Classes_init import df, probamatrix  # Import data & probability matrix
# from Customer_Classes_init file
```

### Question 1.1

The following analysis are conducted in Exploratory.py script.
We are asked what food and drinks are sold by the coffee bar:

```python
print(df['DRINKS'].value_counts())  # Drinks sold
print(df['FOOD'].value_counts())  # Food sold
```

We are then asked how many unique customers the bar had:

```python
print(df['CUSTOMER'].nunique())  # Number of unique customers
```

### Question 1.2

We are then asked to make a bar plot of the total amount of drinks sold over the five years:

```python
# Number of drinks sold by type
df['DRINKS'].value_counts().plot(kind="bar",
                                 title="Number of Drinks Sold by Type 2016-2020",
                                 figsize=(5, 5), color=['#034694', '#001C58', '#5CBFEB', '#D00027',
                                                        '#EF0107', '#DA020E'])
plt.xlabel("Drinks")
plt.ylabel("Total Sales")
drinkcolours = ['#034694', '#001C58', '#5CBFEB', '#D00027',
                '#EF0107', '#DA020E']
plt.tight_layout()
plt.show()
```

And also a bar plot of the total amount of food sold over the five years:

```python
# Number of food sold by type
df['FOOD'].value_counts().plot(kind="bar",
                               title="Number of Food Items Sold by Type 2016-2020",
                               figsize=(5, 5), color=['#034694', '#001C58', '#5CBFEB', '#D00027'])
plt.xlabel("Foods")
plt.ylabel("Total Sales")
plt.tight_layout()
plt.show()
```

We make an additional chart of coffee probability consumption:
```python
# Coffee consumption throughout the day
probamatrix['coffee'].plot(title='Coffee probability consumption throughout the day', figsize=(10, 5), color='C2')
plt.xlabel('Coffee probability consumption')
plt.ylabel('Time')
plt.tight_layout()
plt.show
```
### Question 1.3

We are then asked to determine the probability that a customer buys a certain food 
or drink at any given time.

We do this by creating a matrix (see Customer_Classes_init.py for the code) that will display each purchasing 
probability for every unique fixed purchasing timeslot. The fixed timeslots are every five minutes
between 8:00 am and 11:00 am, every two minutes between 11:00 am and 13:00 pm and
every four minutes between 13:00 pm and 18:00 pm. The last timeslot in the day is
17:56 pm as it includes purchases from 17:56 pm to 18:00 pm.

The probabilities are taken from the probability matrix and displayed in sentence form below:

```python
def averageatanytime():
    data = probamatrix  # we use the constructed probability matrix
    timeslots = data.index.tolist()  # Extract hour and minutes of every fixed purchasing timeslots
    data_trans = data.transpose()  # transpose the matrix
    # Display for each given time the probabilities into a sentence.
    for i in range(0, len(timeslots)):
        list_proba = data_trans[timeslots[i]].tolist()  # Convert the transposed columns into a list of probabilities
        print("On average the probability for a customer at %s buying coffee is %s, "
              "frappucino is %s, milkshake is %s, soda is %s, tea is %s, water is %s. "
              "And for food: %s for cookie, %s for muffin, %s for pie, %s for sandwich and nothing is %s."
              % (timeslots[i], str(round(int(list_proba[0]), 2)) + "%", str(round(int(list_proba[1]), 2)) + "%",
                 str(round(int(list_proba[2]), 2)) + "%", str(round(int(list_proba[3]), 2)) + "%",
                 str(round(int(list_proba[4]), 2)) + "%", str(round(int(list_proba[5]), 2)) + "%",
                 str(round(int(list_proba[6]), 2)) + "%", str(round(int(list_proba[7]), 2)) + "%",
                 str(round(int(list_proba[8]), 2)) + "%", str(round(int(list_proba[9]), 2)) + "%",
                 str(round(int(list_proba[10]), 2)) + "%"))
```

The probability matrix implemented in averageatanytime is below. We use a groupby function to determine the 
food and drink probabilites and then transform the output into a database. We then store the final probability
matrix and its transposed version.  

```python
def display_probamatrix(data):
    # Store the input dataset
    df_copy = data.copy()
    df_copy['hour'] = df_copy.index.strftime('%H:%M')  # Create a column corresponding to only hour and minutes
    df_copy['FOOD'].fillna('nothing', inplace=True)
    # Create a dataframe of probability for drinks
    drinks_proba = df_copy.groupby(['hour']).apply(lambda x: x['DRINKS'].value_counts(normalize=True))  # Generate proba
    drinks_proba = pd.DataFrame(drinks_proba * 100).stack().unstack(1, fill_value=0)  # Transform into dataframe
    drinks_proba.index = df_copy['hour'].value_counts(dropna=False).sort_index().index.tolist()  # Correct index

    # Create a dataframe of probability for food
    food_proba = df_copy.groupby(['hour']).apply(lambda x: x['FOOD'].value_counts(normalize=True, dropna=False))
    # Generate proba
    food_proba = pd.DataFrame(food_proba * 100).stack().unstack(1, fill_value=0)  # Transform into dataframe
    food_proba.index = df_copy['hour'].value_counts(dropna=False).sort_index().index.tolist()  # Correct index

    df_proba = pd.concat([drinks_proba, food_proba], axis=1, sort=False, join='outer')
    return df_proba

probamatrix = display_probamatrix(df)  # Storing the probamatrix
probamatrix_transposed = probamatrix.transpose()  # Storing the transposed probamatrix
```

## PART 2: CREATE CUSTOMERS

The following analysis are conducted in Customer_Classes.py script.

**Preliminary information**:

We are asked in this part to use the probabilities determined in Question 1.3 to create
customers who buy following these probabilities. 

We are told that the coffee bar gets a range of customers. Each customer has an ID and 
a certain budget. There are two global types of customers: returning customers and one-time
customers. One-time customers have a budget of 100 euros and there are two types: regulars
and customers that have found the coffee bar through Tripadvisor. Customers having found the
bar through Tripadvisor give a random tip between 1 to 10 euros. Returning customers also have
two types: regulars who have a budget of 250 euros and hipsters who have a budget of 500 euros.

We are told that all customers are able to buy drinks when given the correct probability at that time, 
the price of the food and drinks and are able to “tell” what they have bought (separate for food and drinks) 
and the amount they payed. Returning customers keep track of their entire history of purchases 
(food, drinks and budget). 

**Create Customers**:

We begin by importing the data and the transposed probability matrix stored in Customer_Classes_init.py.
We then create a "Cafe" class the represents the Coffee shop and stores its purchasing menu. The menu is defined
by lists (drinks, food, drink types, etc.) and the budget constraint is also integrated (customers cannot go to 
the cafe if they cannot afford the most expensive menu). 

The consumption is determined by a "consumption" function that determines the food and drink purchases based on the
purchasing timeslot and using the probabilities stored in the transposed matrix. The output of this function is a list 
containing for each timeslot: the drink purchased, the cost of the drink purchased, the food item purchased (or 
"nothing" if nothing purchased) and the cost of the food item purchased if applicable.

```python
import random as rm
from Code.Customer_Classes_init import probamatrix_transposed

# Class representing the coffee bar menu
class Cafe: 
    menu_drinks = {}
    menu_food = {}
    drinks_types = []
    food_types = []
    drinks_prices = []
    food_prices = []
    most_expensive_menu = 0

    @classmethod
    def menu(cls, drinks, food):
        cls.menu_drinks = drinks
        cls.menu_food = food
        cls.drinks_types = [*cls.menu_drinks]
        cls.food_types = [*cls.menu_food]
        cls.drinks_prices = list(cls.menu_drinks.values())
        cls.food_prices = list(cls.menu_food.values())
        cls.most_expensive_menu = max(Cafe.food_prices) + max(Cafe.drinks_prices)

# Consumption function
    @staticmethod
    @staticmethod
    def consumption(purchasinglist):
        cons = []  # Initialising the output consumption list
        for inc in range(0, len(purchasinglist)):  # Loop for each timeslot in the input purchasing timeslots
            timeslot_selected = purchasinglist[inc]  # Store the selected timeslot
            list_proba = probamatrix_transposed[timeslot_selected].tolist()  # Extract from the transposed matrix
            # the probabilities corresponding to the input timeslot
            buydrk = rm.choices(Cafe.drinks_types, list_proba[0:6])[0]  # Extract resulting drink
            # according to these probabilities
            buyfoo = rm.choices(Cafe.food_types, list_proba[6:11])[0]  # Extract resulting food
            # according to these probabilities
            pricedrk = Cafe.menu_drinks.get(buydrk)  # Extract the price of the resulting drink
            pricefoo = Cafe.menu_food.get(buyfoo)  # Extract the price of the resulting food
            cons.append([timeslot_selected, buydrk, pricedrk, buyfoo, pricefoo])
            # Add to the output consumption list,
            # for each input timeslot, a list containing the timeslot,
            # resulting drink and its price, resulting food and its price
        return cons


```

We then create an "Archive" class that stores customer information (ID, purchasing timeslots and consumptions).
This information can be retrieved given a customer ID using the "consult_historic" function. A function reset is
implemented if we want to reset those databases.

```python
class Archive:
    database_id_time = {}  # Create dictionary linking customers' ID (key) to their purchasing timeslots (value)
    database_id_cons = {}  # Create dictionary linking customers' ID (key) to their consumptions (value)

# Information retrieval function
    @staticmethod
    def consult_historic(database, key):
        return database[key]
    
    @classmethod
    def reset(cls):
        cls.database_id_time = {}  # Reset dictionary linking customers' ID (key) to their purchasing timeslots (value)
        cls.database_id_cons = {}  # Reset dictionary linking customers' ID (key) to their consumptions (value)
```

However, we need to be sure that we do not create multiple keys for the same returning customer, so 
we create a function that checks whether the key is already registered in the dictionary after inputting
a dictionary, a key and a value. If it is not already registered, the key is added with its value and
the value is returned. Otherwise, ie for returning customers, the key is redefined as the new value
and the value returned is the additional value (ie the length of the new value minus the length of the
original key).

```python
# Function that deals with the case of returning customers:

def check_key(database, key, value): #checks whether the key is already registered
    if key not in database: #if not, adds to dictionary and returns value
        database[key] = value
        return database[key] 
    else: #if a returning customer
        additional_value = len(value) - len(database[key])
        database[key] = value #new value
        return additional_value #additional value
```

We now create a new function "adding" that takes a key and value and returns the consumption history linked
to this key. The consumption function is used to calculate the consumption of new and returning customers. 
New customers are determined because their key is not already in the database linking customer IDs and timeslots. 
If this is the case, their consumption is computed and stored in the database linking customer IDs and 
consumptions. The consumption history of the new customer is the output. If the key is already in the 
ID-Timeslots database, then we determine that it is a returning customer. In this case, the new consumption 
is added to the prior consumption. The output is the consumption history.

```python
# Creating a function returns the consumption history linked to the customer (new or returning):

def adding(key, value):
    if key not in Archive.database_id_time:  # New customer: key not already in ID-Timeslots database
        Archive.database_id_cons[key] = Cafe.consumption(check_key(Archive.database_id_time, key, value))  # Compute
        # the consumption and register it in dictionary linking ID to consumptions
        return Archive.database_id_cons[key]  # Return consumption historic
    else:  # Returning customers: If already registered in dictionary linking ID to Timeslots
        Archive.database_id_cons[key] = Cafe.consumption(value[-check_key(Archive.database_id_time, key, value):]) + \
                                        Archive.database_id_cons[key]  # Compute the additional consumption and add it
        # to the consumption historic
        return Archive.database_id_cons[key]  # Return consumption historic
```

We then use classes to set up our simulation. Our highest level class is the Customer class. This class uses the 
"adding" function from above to determine whether the client is new or returning and to compute their consumption,
defined as the attribute "ind_cons" within the class. A list is then created to taken in initial customer budgets.

```python
class Customer(object):
    def __init__(self, ID, purchasinglist):
        self.ID = ID
        self.purchasinglist = purchasinglist
        self.ind_cons = adding(self.ID, self.purchasinglist)

    customer_budgets = {}

    # Creating a function that defines customers' initial budget and stores it in customer_budgets  dictionary
    @classmethod
    def budget(cls, x):
            cls.customer_budgets = x
```

Subclasses are then created for each type of customer: "Once" for one-time customers, "Trip" for one-time Tripadvisor
customers, "Return" for regular returning customers and "Hipster" for returning Hipster customers. Their different 
budgets and specificities (tip for Trip) are used to calculate their remaining budget.

```python
class Once(Customer):
    def budget_remain(self):  # Creating a function that returns customer's current budget
        budget = Customer.customer_budgets['Once']  # Initialize its budget
        for i in range(0, len(self.ind_cons)):  # Extract from each consumption drink & food prices
            budget = budget - self.ind_cons[i][2] - self.ind_cons[i][4]
        return budget

class Trip(Once):
    def budget_remain(self):  # Creating a function that returns customer's current budget
        budget = Customer.customer_budgets['Trip']  # Initialize its budget
        for i in range(0, len(self.ind_cons)):  # Extract from each consumption drink & food prices
            tip = rm.randrange(1, 10)  # Define a random tip between 1 and 10 for each consumption
            budget = budget - self.ind_cons[i][2] - self.ind_cons[i][4] - tip
        return budget

class Return(Customer):
    def budget_remain(self):  # Creating a function that returns customer's current budget
        budget = Customer.customer_budgets['Return']  # Initialize its budget
        for i in range(0, len(self.ind_cons)):  # Extract from each consumption drink & food prices
            budget = budget - self.ind_cons[i][2] - self.ind_cons[i][4]
        return budget

class Hipster(Return):
    def budget_remain(self):  # Creating function that returns customer's current budget
        budget = Customer.customer_budgets['Hipster']  # Initialize its budget
        for i in range(0, len(self.ind_cons)):  # Extract from each consumption drink & food prices
            budget = budget - self.ind_cons[i][2] - self.ind_cons[i][4]
        return budget
```

## PART 3: SIMULATIONS

**Preliminary information**:

In this part, we are asked to create a simulation for the day and to run it for five years (ie run the daily simulation
over a period of five years). We are told that there are 1000 returning customers of which one third are hipsters. 
There is a 20% chance to have one of the 1000 returning customers (chosen randomly). We are then told that about a 
tenth of the one-time customers are from Tripadvisor. There is about a 80% chance to have a one-time customer. 

Every customer should have a unique ID and their budget lowers every time they buy something. If a returning customer 
is no longer able to buy the most expensive food and drink, they can no longer come to the coffee bar. 
  
The prices for the food are: sandwich = 2, cookie = 2 and the rest 3.
The prices for the drinks are: milkshake = 5, frappucino = 4, water = 2 and the rest 3.

After the 5-year simulation, we are asked to make some plots. 

**Setting up the simulation**:

We create a class "Clientele" to set up the customer generation process. A set is created that will contain the unique
ID of one-timer customers, a list is created for the returning customers and a dictionary for returning customers with
ID as the key. A function "generate_returning" is then used to generate the number of returning customers with the 
proportions given (2/3 are regular returning customers and 1/3 are hipsters). Lists are then filled with a unique 
customer ID for the returning regular customers (id is "RReg" + index number) and returning hipster customers (id
is "RHip" + index number). A function reset is implemented if we want to reset those databases.

```python
# Creating 1000 return IDs with 667 regular returning customers and 333 Hipsters
# timeslots as value

class Clientele:
    set_once = set()  # Initializing a set that will contain every unique ID of one-timers
    list_return_ID = []  # Initialize the list of the returning customers
    dict_return_id_time = {}  # Initialize the dictionary of theses customers with ID as key and purchasing

    @classmethod
    def generate_returning(cls, number_returning):
        from math import ceil
        limit = ceil((2 / 3) * number_returning)
        for index in range(0, limit):
            return_id = 'RReg' + str(index)  # Store the fact that the returning customer is regular
            cls.dict_return_id_time[return_id] = []  # Initialize their purchasing timeslots
            cls.list_return_ID.append(return_id)  # Fill the list of 1000 returning with unique IDs of regulars
        for index in range(limit, number_returning):
            return_id = 'RHip' + str(index)  # Store the fact that the returning customer is an hipster
            cls.dict_return_id_time[return_id] = []  # Initialize their purchasing timeslots
            cls.list_return_ID.append(return_id)  # Fill the list of 1000 returning with unique IDs of hipsters

    @classmethod
    def reset(cls):
        cls.set_once = set()  # Reset the set that will contain every unique ID of one-timers
        cls.list_return_ID = []  # Reset the list of the returning customers
        cls.dict_return_id_time = {}  # Reset the dictionary of theses customers with ID as key and purchasing
```

We then create a function "customer-type" that returns a customer type according to the given probabilities.

```python
# Customer type random selection function:

def customer_type():
    population_customer = ['Return', 'Hipster', 'Once', 'Trip']
    weight_customer = [(2 / 3) * (1 / 5), (1 / 3) * (1 / 5), (9 / 10) * (4 / 5), (1 / 10) * (4 / 5)]
    random_customer_type = rm.choices(population_customer, weight_customer)
    return random_customer_type[0]
```

We then create a function "customer_id" that takes the type of customer and returns a new ID if it is a one-time
customer, and a random ID from the returning customer list otherwise. 

```python
# Customer ID generator function:

def customer_id(type_customer):
    if type_customer == 'Return' or type_customer == 'Hipster':
        returning_id = rm.choice(Clientele.list_return_ID)
        return returning_id
    else:
        once_id = "OneTimer" + str(len(Clientele.set_once))
        Clientele.set_once.add(once_id)
        return once_id
```

We then create the "simulation" function to run our simulation. 

We create a "simulation" function that begins by initializing the coffee bar menu, the customers budget and the number
of loyal customers (returning customers). In addition the Archive databases and the Clientele databases can be reset. 
An new empty dataframe is then created with the same structure as the 
original one. We then loop over the dataset filling the index with the timeslot, generating the customer type with the 
"customer.type" function, generating a customer ID if it is a returning customer and then filling the empty
customer ID column with the ID. 

The next steps depend on the type of customer. For returning customers, the previous timeslots are retrieved
and updated. The consumption is calculated via the Customer class ind_cons attribute, which integrates the "adding"
function (that uses the original "consump" function). Lastly, the budget constraint is checked to be sure that the
returning customer has enough budget to purchase the most expensive menu. As noted before, Hipsters have a larger 
budget (500 euros) than returning regular customers (250 euros). For one-time customers, the consumption is calculated
as for the returning customers and there is no need to check the budget constraint (as they have enough budget to 
purchase the most expensive menu and do not have any consumption history).

We finish by filling the new dataset columns with the data (ID, drinks and food).

```python
def simulation(data, drinks_menu, food_menu, budget_customer, number_loyal,
               reset_archive, reset_Clientele):
    # If we need to reset the previous Clientele database and Coffee shop archives
    if reset_archive == 1:
        Archive.reset()
    if reset_Clientele == 1:
        Clientele.reset()
        Clientele.generate_returning(number_loyal)  # Create the list of loyal customers
    # Initialize Coffee shop menu, customers' budget, and the number of loyal customers
    Cafe.menu(drinks_menu, food_menu)
    Customer.budget(budget_customer)

    # Create an empty dataframe with the same structure of the original one
    data = pd.DataFrame().reindex_like(data)
    # Initialize its future columns
    col_simulation_customer = []
    col_simulation_drinks = []
    col_simulation_food = []

    # Fill these columns
    for index in range(0, len(data.index)):  # Fill for each 'row' (index in this case) of the dataset
        timeslot_selected = [data.index[index].strftime('%H:%M')]  # Extract the corresponding 'row' (index)
        my_type = customer_type()  # Generate the type of the customer
        my_id = customer_id(my_type)  # Generate its id (retrieve if not new)
        col_simulation_customer.append(my_id)  # Fill the customer column with its id

        if my_id[0:4] == 'RReg':  # check whether the customer is Regular Return type
            old_buy_timeslots = Clientele.dict_return_id_time.get(my_id)  # Get customer previous purchasing timeslots
            current_buy_timeslots = old_buy_timeslots + timeslot_selected  # Define customer current
            # purchasing timeslots
            Clientele.dict_return_id_time[my_id] = current_buy_timeslots  # Update timeslots in customer dictionary
            my_customer = Return(my_id, current_buy_timeslots)  # Define customer Return class
            col_simulation_drinks.append(my_customer.ind_cons[len(current_buy_timeslots) - 1][1])  # Fill the
            # drinks column with the type of drinks consumed retrieve by .ind_cons method
            col_simulation_food.append(my_customer.ind_cons[len(current_buy_timeslots) - 1][3])  # Fill the
            # food column with the type of food consumed retrieve by .ind_cons method
            if my_customer.budget_remain() < Cafe.most_expensive_menu:  # Budget Constraint
                Clientele.list_return_ID.remove(my_id)

        elif my_id[0:4] == 'RHip':  # check whether the customer is Hipster Return type
            old_buy_timeslots = Clientele.dict_return_id_time.get(my_id)  # Get customer previous purchasing timeslots
            current_buy_timeslots = old_buy_timeslots + timeslot_selected  # Define customer current
            # purchasing timeslots
            Clientele.dict_return_id_time[my_id] = current_buy_timeslots  # Update timeslots in customer dictionary
            my_customer = Hipster(my_id, current_buy_timeslots)  # Define customer Hipster class
            col_simulation_drinks.append(my_customer.ind_cons[len(current_buy_timeslots) - 1][1])  # Fill the
            # drinks column with the type of drinks consumed retrieve by .ind_cons method
            col_simulation_food.append(my_customer.ind_cons[len(current_buy_timeslots) - 1][3])  # Fill the
            # food column with the type of food consumed retrieve by .ind_cons method
            if my_customer.budget_remain() < Cafe.most_expensive_menu:  # Budget Constraint
                Clientele.list_return_ID.remove(my_id)

        elif my_type == 'Once':  # check whether the customer is Regular Once type
            my_customer = Once(my_id, timeslot_selected)  # Define customer Once class
            col_simulation_drinks.append(my_customer.ind_cons[0][1])  # Fill the
            # drinks column with the type of drinks consumed retrieve by .ind_cons method
            col_simulation_food.append(my_customer.ind_cons[0][3])  # Fill the
            # food column with the type of food consumed retrieve by .ind_cons method

        else:  # when the customer is Trip Advisor Once type
            my_customer = Trip(my_id, timeslot_selected)  # Define customer Trip class
            col_simulation_drinks.append(my_customer.ind_cons[0][1])  # Fill the
            # drinks column with the type of drinks consumed retrieve by .ind_cons method
            col_simulation_food.append(my_customer.ind_cons[0][3])  # Fill the
            # food column with the type of food consumed retrieve by .ind_cons method

    # Fill the empty structured dataset by column
    data['CUSTOMER'] = col_simulation_customer
    data['DRINKS'] = col_simulation_drinks
    data['FOOD'] = col_simulation_food
    return data
```

Before running the simulation, we create a profit function detailed below:

```python
# Create a profit function that given a coffee shop dataset, and its menu,
# returns in a list the average daily revenue per consumption, daily revenue and average daily revenue

def profit(data, menu_drinks, menu_food):
    data = data.copy()
    data['FOOD'] = data['FOOD'].replace(np.nan, 'nothing')  # Replace the missing values
    data['DRINKS'].replace(menu_drinks, inplace=True)  # Replace each drinks type by its price
    data['FOOD'].replace(menu_food, inplace=True)  # Replace each food type by its price
    data['revenue'] = data['DRINKS'] + data['FOOD']  # Compute the revenue for each consumption

    # Compute the average daily revenue per consumption, daily revenue, and average daily revenue
    average_daily_revenue_per_consumption = data.groupby(data.index.strftime('%d/%m/%Y'))['revenue'].agg('mean')
    daily_revenue = data.groupby(data.index.strftime('%d/%m/%Y'))['revenue'].agg('sum')
    average_daily_revenue = sum(daily_revenue.tolist()) / len(daily_revenue.tolist())

    return [average_daily_revenue_per_consumption, daily_revenue, average_daily_revenue]
```


**Running the simulation**

We know run a simulation with the given price, budget and customer parameters.

We then run a simulation process using the "simulation" function.

```python
# Simulation process

customer_budget = {'Return': 250, 'Hipster': 500, 'Once': 100, 'Trip': 100}
menu_drinks = {'coffee': 3, 'frappucino': 4, 'milkshake': 5, 'soda': 3, 'tea': 3, 'water': 2}
menu_food = {'cookie': 2, 'muffin': 3, 'nothing': 0, 'pie': 3, 'sandwich': 2}
number_returning = 1000

df_simulation = simulation(df, menu_drinks, menu_food, customer_budget, number_returning,
                           reset_archive=1, reset_Clientele=1)
```

**Creating plots**:

We can now check visually if our simulation worked correctly by checking the same plots as done in 1.2 for the 
original data. 

We create two plots comparing the total drinks sold by type in the original data and the simulated data. 
The total drinks sold by type is very close in the two datasets.  

```python
# Comparison with graphs in 1.2

colors = ['#034694', '#001C58', '#5CBFEB', '#D00027', '#EF0107', '#DA020E']

# Graph 1

plt.subplot(1, 2, 1)
df['DRINKS'].value_counts().plot(kind="bar",
                                 title="Total Drinks Sold - Real Data",
                                 figsize=(10, 5), color=colors)
plt.xlabel("Drinks")
plt.ylabel("Total Sales")

plt.subplot(1, 2, 2)
df_simulation['DRINKS'].value_counts().plot(kind="bar",
                                            title="Total Drinks Sold - Simulation Data",
                                            figsize=(10, 5), color=colors)
plt.xlabel("Drinks")
plt.ylabel("Total Sales")
plt.tight_layout()
plt.show()

# Graph 2

df_simulation.loc[df_simulation['FOOD'] == 'nothing', 'FOOD'] = np.nan  # Put missing values instead of 'nothing'

plt.subplot(1, 2, 1)
df['FOOD'].value_counts().plot(kind="bar",
                               title="Total Food Items Sold - Real Data",
                               figsize=(10, 5), color=colors)
plt.xlabel("Food")
plt.ylabel("Total Sales")

plt.subplot(1, 2, 2)
df_simulation['FOOD'].value_counts().plot(kind="bar",
                                          title="Total Food Items Sold - Simulation Data",
                                          figsize=(10, 5), color=colors)
plt.xlabel("Food")
plt.ylabel("Total Sales")
plt.tight_layout()
plt.show()
```

We then compute the average income per purchase, the daily income and the average daily income using the profit function
(see Simulation_init for the code)

```python
# Compute the average daily revenue

# Compute the average daily revenue for initial dataset
df_profit = profit(df, menu_drinks, menu_food)

# Compute the average daily revenue for simulation
df_simulation_profit = profit(df_simulation, menu_drinks, menu_food)
```


The daily income of the original and simulated data is compared in the below plot. There are very similar. 

```python
#Plot
# Plot

profits_real = df_profit[1]  # Real daily revenue
profits_sim = df_simulation_profit[1]  # Simulated daily revenue

plt.subplot(2, 1, 1)
profits_real.plot(kind="line",
                  title="Daily Profits - Real Data",
                  figsize=(10, 5))
plt.xlabel("")
plt.ylabel("Profits")
plt.xticks([])

plt.subplot(2, 1, 2)
profits_sim.plot(kind="line",
                 title="Daily Profits - Simulated Data",
                 figsize=(10, 5))
plt.xlabel("Date")
plt.ylabel("Profits")
plt.show()
```

## PART 4: ADDITIONAL QUESTIONS

We are asked some additional questions in this part.

**Question 1**: Show some buying histories for returning customers for your simulations.

We run the simulation and choose randomly several returning customer buying histories to show.

```python
customer_budget = {'Return': 250, 'Hipster': 500, 'Once': 100, 'Trip': 100}
menu_drinks = {'coffee': 3, 'frappucino': 4, 'milkshake': 5, 'soda': 3, 'tea': 3, 'water': 2}
menu_food = {'cookie': 2, 'muffin': 3, 'nothing': 0, 'pie': 3, 'sandwich': 2}
number_returning = 1000

df_simulation1 = simulation(df, menu_drinks, menu_food, customer_budget, number_returning,
                            reset_archive=1, reset_Clientele=1)

# Examples: Random Buying histories of returning customer
print(Archive.database_id_cons[rd.choice(list(Clientele.dict_return_id_time))])
print(Archive.database_id_cons[rd.choice(list(Clientele.dict_return_id_time))])
print(Archive.database_id_cons[rd.choice(list(Clientele.dict_return_id_time))])
```

**Question 2** is composed of a series of questions on returning customers in our original dataset.

How many returning customers are there?

We create a function "count_frequency" that counts the frequency of elements in a given list and returns the 
result through a dictionary. We then apply the function to the customer list and extract only the returning 
customers. We then display the number of returning customers. There are 1000 in the original dataset.  

```python
def count_frequency(my_list):  # Creating a function that count the frequency of elements in a given list
    # and returns the result through a dictionary
    my_dict = {}
    for index in my_list:
        if index in my_dict:
            my_dict[index] = 1 + my_dict[index]
        else:
            my_dict[index] = 1
    return my_dict


data = df.copy()  # Store the initial dataset
freq_customer = count_frequency(data['CUSTOMER'])  # Apply the function to the customer list
freq_return = {key: val for key, val in freq_customer.items() if val != 1}  # Extract only the returning customers

number_returning = len(freq_return)  # Number of returning customers
print(number_returning)  # 1000 actual returning customers
```

Do they have specific times when they show up more?
Can you determine a probability of having a onetime or returning customer at a given time?

We initialize the binary list for one-times and fill the list with 1 or 0 for one-time or not respectively.
We add this dummy column to the dataset. We then determine the probability of having a returning customer at 
each timeslot saved in the "proba-return" variable. This responds to the second of the two questions above.

We then output the ten times when returning customers show up the most and the least. The ten most popular
timeslots are in the afternoon between 13:00 and 18:00 and the ten least popular timeslots are all in the end of 
the morning. This reponds to the first of the two questions above.

```python
binary_returning = []  # Initialize the list storing the fact whether a customer is a one-timer or not
for i in range(0, len(data['CUSTOMER'])):  # Fill the list with 0 or 1 whether the customer is a one-timer or not
    if data['CUSTOMER'][i] in freq_return:
        binary_returning.append(1)
    else:
        binary_returning.append(0)

data['binary_returning'] = binary_returning  # Create a new column in the dataset of this dummy variable

proba_return = data.groupby(data.index.strftime('%H:%M')).apply(lambda x: x['binary_returning'].
                                                                value_counts(normalize=True))[1]
print(proba_return)  # We obtain the probability of having a returning customer at each timeslot

proba_return_given_time = dict(zip(proba_return.index.tolist(), proba_return.values.tolist()))  # Into dictionary
print(proba_return_given_time)  # Probability that they show up at a certain time are shown in proba_return_given_time

maxi = sorted(proba_return_given_time, key=proba_return_given_time.get, reverse=True)[:10]
print(maxi)  # The ten most-likely timeslots for returning customers are all in the afternoon between 13:00 and 18:00
mini = sorted(proba_return_given_time, key=proba_return_given_time.get, reverse=False)[:10]
print(mini)  # The ten least popular timeslots for returning customers are all in the end of the morning
```

How does this impact their buying history?
Do you see correlations between what returning customers buy and one-timers?

We create a dataset for one-timers and for returners. We compute their consumption probabilities separately and 
then take the difference between these probabilities in the variable "difference_once_return". These three probability
sets are displayed below. We then do some analytics below to respond to the two above questions. 

```python
df_one_timer = data.copy()
df_one_timer = df_one_timer[df_one_timer['binary_returning'] == 0]  # Create a dataset of only one-timers

df_returning = data.copy()
df_returning = df_returning[df_returning['binary_returning'] == 1]  # Create a dataset of only returners

proba_one_timer = display_probamatrix(df_one_timer)  # Compute one-timers probabilities
proba_returning = display_probamatrix(df_returning)  # Compute returners probabilities
difference_once_return = proba_one_timer.subtract(proba_returning)  # Difference in probabilities

print(proba_one_timer)  # One-time customers consumption probabilities by timeslot
print(proba_returning)  # Returning customers consumption probabilities by timeslot
print(difference_once_return)  # Difference in probabilities
```

We display a plot showing the difference in consumption probabilities between one-timers and returning customers. 
We also calculate the average difference in probabilities by product, shown in "behaviour_difference". We can see
clearly that returning customers have a much higher percentage of consuming coffee in the morning compared to one-time 
customers. One-time customers have a higher chance of consuming everything else. Then starting around 11:00 the 
probability distributions become similar and there do not seem to be major differences between consumption patterns of 
one-time and returning customers. It is possible that returning customers consume more coffee in the morning because
they come often and prefer buying food items during more regular eating or snacking times (lunch time or afternoon),
while one-time customers only come once and want to try different things on the menu, regardless of the time. This
responds to the above two questions.

```python
difference_once_return.plot(title='Product consumption probability difference '
                                  '(One-timers - returning) over a day')
plt.ylabel('Probability difference (%)')
plt.xlabel('Time')
plt.show()

behaviour_difference = {}
for i in range(0, len(difference_once_return.columns.tolist())):
    column_name = difference_once_return.columns.tolist()[i]
    average_difference = sum(difference_once_return[column_name]) / len(difference_once_return[column_name])
    behaviour_difference[column_name] = average_difference

print(behaviour_difference)  # Overall Correlation between returning customer and one-timers
# (Returners significantly consume more coffee and less others drinks)
```

**Question 3**: Consequences of reducing returning customers to 50 and simulating the same period

We run the same simulation but with only 50 returning customers. The simulation process stops at some point because all
returning customers have spent all their budget before the end of the simulation. This is because once a returning type
is randomly chosen, the pool of returning customers available is empty, and the simulation stops. We therefore do not
show the error message and move on to question 4. 

```python
customer_budget = {'Return': 250, 'Hipster': 500, 'Once': 100, 'Trip': 100}
menu_drinks = {'coffee': 3, 'frappucino': 4, 'milkshake': 5, 'soda': 3, 'tea': 3, 'water': 2}
menu_food = {'cookie': 2, 'muffin': 3, 'nothing': 0, 'pie': 3, 'sandwich': 2}
number_returning = 50

try:
    df_simulation2 = simulation(df, menu_drinks, menu_food, customer_budget, number_returning,
                                reset_archive=1, reset_Clientele=1)
except IndexError:
    print("Returning customers have spent all budget before end of simulation. "
          "Simulation therefore stops before the end. Moving onto Question 4.")
```

**Question 4**: The prices change from the beginning of 2018 and go up by 20%.

We alter the prices beginning in 2018 and rerun the simulation. We also run the simulation with the original prices.
We then compare the available remaining returning customers and the average daily income. Raising prices increases
the daily income of the bar by about 90 euros per day and actually decreased the number of remaining returning
customers (by 140). The increase in the daily income is simply due to the fact that we
do not include supply costs for the coffee bar in this simulation, so an increase in prices leads to an increase in 
profits as long as there are enough customers and they do not change their purchasing habits (which is the case for
this inflation simulation). The decrease in the available returning customers is simply due to the fact that they spend 
their budget faster (as drinks and food are more expensive) and that their budget constraint is more strict 
(since the most expensive menu costs more). 

```python
customer_budget = {'Return': 250, 'Hipster': 500, 'Once': 100, 'Trip': 100}
menu_drinks = {'coffee': 3, 'frappucino': 4, 'milkshake': 5, 'soda': 3, 'tea': 3, 'water': 2}
menu_food = {'cookie': 2, 'muffin': 3, 'nothing': 0, 'pie': 3, 'sandwich': 2}
menu_drinks_after = {'coffee': 3.6, 'frappucino': 4.8, 'milkshake': 6, 'soda': 3.6, 'tea': 3.6, 'water': 2.4}
menu_food_after = {'cookie': 2.4, 'muffin': 3.6, 'nothing': 0, 'pie': 3.6, 'sandwich': 2.4}
number_returning = 1000

# Placebo simulation
df_simulation3_placebo = simulation(df, menu_drinks, menu_food, customer_budget, number_returning,
                                    reset_archive=1, reset_Clientele=1)
nb_remaining_placebo = len(Clientele.list_return_ID)  # Remaining available returning customers for placebo

# Inflation simulation
df_simulation3_before = simulation(df[df.index.strftime('%Y') < "2018"], menu_drinks, menu_food,
                                   customer_budget, number_returning, reset_archive=1, reset_Clientele=1)
df_simulation3_after = simulation(df[df.index.strftime('%Y') >= "2018"], menu_drinks_after, menu_food_after,
                                  customer_budget, number_returning, reset_archive=0, reset_Clientele=0)
df_simulation_inflation = pd.concat([df_simulation3_before, df_simulation3_after])  # Merge the two datasets
nb_remaining_inflation = len(Clientele.list_return_ID)  # Remaining available returning customers for inflation

# Compare both simulation
print(nb_remaining_placebo - nb_remaining_inflation)  # Compare available remaining returning customers ...
print(profit(df_simulation3_placebo, menu_drinks, menu_food)[2] -  # ... and average daily income
      0.4 * profit(df_simulation3_before, menu_drinks, menu_food)[2] -
      0.6 * profit(df_simulation3_after, menu_drinks_after, menu_food_after)[2])```
```

**Question 5**: The budget of hipsters drops to 40.

We run the simulation below with the hipster budget set at 40. As in question 3, returning customers have spent all 
their budget before the end of the simulation. The simulation therefore stops. We do not show this error message and 
move on to question 6.

```python
customer_budget = {'Return': 250, 'Hipster': 40, 'Once': 100, 'Trip': 100}
menu_drinks = {'coffee': 3, 'frappucino': 4, 'milkshake': 5, 'soda': 3, 'tea': 3, 'water': 2}
menu_food = {'cookie': 2, 'muffin': 3, 'nothing': 0, 'pie': 3, 'sandwich': 2}
number_returning = 1000

try:
    df_simulation4 = simulation(df, menu_drinks, menu_food, customer_budget, number_returning,
                                reset_archive=1, reset_Clientele=1)
except IndexError:
    print("Returning customers have spent all budget before end of simulation. "
          "Simulation therefore stops before the end. Moving onto Question 6.")

# The simulation process stops at some point.  At this point, all returning customers' have spent
# all their budget before the end of the simulation.
# Once a returning type is randomly chosen, the pool of returning customer available is empty.
# Thus the simulation process stops.
```

**Question 6**: The prices of coffee bars are regulated by the City mayor in coordination with the coffee bar owners.
During the city mayoral election in 2016, one candidate (he lost :( (due to massive fraud in mail-in-ballots (obviously)) 
wanted to increase coffee bar income. The mayor therefore asked the coffee bars to choose the drink for which they 
would like a 1-euro price increase to apply. Which product would the coffee bars choose if this policy were implemented.

To answer this question, we create a function that returns the daily average profit from each different simulation 
corresponding to each time a drink price goes up by 1 euro. We determine that coffee bars would have chosen to increase
the price of soda, as this would create the most income. 

```python
customer_budget = {'Return': 250, 'Hipster': 500, 'Once': 100, 'Trip': 100}
menu_drinks = {'coffee': 3, 'frappucino': 4, 'milkshake': 5, 'soda': 3, 'tea': 3, 'water': 2}
menu_food = {'cookie': 2, 'muffin': 3, 'nothing': 0, 'pie': 3, 'sandwich': 2}
number_returning = 1000

# Creating a function that returns the daily average profit from each different simulation corresponding
# to each time a product price goes up by 1 euro.

def policy_one_euro(drinks, food):
    dict_average_daily_profit = {}  # Initialise the dictionary returning the profit
    offer_food = food
    for index in range(0, len(drinks)):  # Looping for 6 different simulations for the 6 different drinks
        offer_drinks = drinks.copy()
        offer_drinks[[*offer_drinks][index]] = offer_drinks[[*offer_drinks][index]] + 1  # Adding 1$
        increase_drinks = simulation(df, offer_drinks, offer_food, customer_budget, number_returning,
                                     reset_archive=1, reset_Clientele=1)  # Run simulation
        dict_average_daily_profit[[*offer_drinks][index]] = profit(increase_drinks, offer_drinks,
                                                                   offer_food)[2]  # Fill the dictionary
    return dict_average_daily_profit

print(policy_one_euro(menu_drinks, menu_food))  # Run the analysis (duration 5 mn)
# Coffee shop would have increased soda price
```



