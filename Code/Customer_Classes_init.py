import pandas as pd
import os

# We just store our dataset and proba matrix into a proper file

importpath = os.path.abspath("../Data/Coffeebar_2016-2020.csv")
df = pd.read_csv(importpath, sep=';', index_col='TIME', parse_dates=True)


# Create a matrix that will display each purchasing probability for every unique fixed purchasing timeslots


def display_probamatrix(data):
    # Store the input dataset
    df_copy = data.copy()
    df_copy['hour'] = df_copy.index.strftime('%H:%M')  # Create a column corresponding to only hour and minutes
    df_copy['FOOD'].fillna('nothing', inplace=True)
    # Create a dataframe of probability for drinks
    drinks_proba = df_copy.groupby(['hour']).apply(lambda x: x['DRINKS'].value_counts(normalize=True))  # Generate proba
    drinks_proba = pd.DataFrame(drinks_proba * 100).stack().unstack(1, fill_value=0)  # Transform into dataframe
    drinks_proba.index = df_copy['hour'].value_counts(dropna=False).sort_index().index.tolist()  # Correct index

    # Create a dataframe of probability for food
    food_proba = df_copy.groupby(['hour']).apply(lambda x: x['FOOD'].value_counts(normalize=True, dropna=False))
    # Generate proba
    food_proba = pd.DataFrame(food_proba * 100).stack().unstack(1, fill_value=0)  # Transform into dataframe
    food_proba.index = df_copy['hour'].value_counts(dropna=False).sort_index().index.tolist()  # Correct index

    # Concatenate the two dataframes
    df_proba = pd.concat([drinks_proba, food_proba], axis=1, sort=False, join='outer')
    return df_proba


probamatrix = display_probamatrix(df)  # Storing the probamatrix
probamatrix_transposed = probamatrix.transpose()  # Storing the transposed probamatrix
