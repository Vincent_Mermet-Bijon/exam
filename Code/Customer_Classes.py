import random as rm

from Code.Customer_Classes_init import probamatrix_transposed


# Creating a class representing the Coffee shop that stores its menu
# and define the purchasing action through the consumption function

class Cafe:
    # Defining drinks and food menu and storing drinks, food types and prices into lists
    # and compute the most expensive menu for budget constraint in Simulation part
    menu_drinks = {}
    menu_food = {}
    drinks_types = []
    food_types = []
    drinks_prices = []
    food_prices = []
    most_expensive_menu = 0

    @classmethod
    def menu(cls, drinks, food):
        cls.menu_drinks = drinks
        cls.menu_food = food
        cls.drinks_types = [*cls.menu_drinks]
        cls.food_types = [*cls.menu_food]
        cls.drinks_prices = list(cls.menu_drinks.values())
        cls.food_prices = list(cls.menu_food.values())
        cls.most_expensive_menu = max(Cafe.food_prices) + max(Cafe.drinks_prices)

    # Creating a function taking a list of purchasing timeslots and return a list containing for each purchasing
    # timeslot the type and prices of drink or food purchased, according to the probability matrix
    @staticmethod
    def consumption(purchasinglist):
        cons = []  # Initialising the output consumption list
        for inc in range(0, len(purchasinglist)):  # Loop for each timeslot in the input purchasing timeslots
            timeslot_selected = purchasinglist[inc]  # Store the selected timeslot
            list_proba = probamatrix_transposed[timeslot_selected].tolist()  # Extract from the transposed matrix
            # the probabilities corresponding to the input timeslot
            buydrk = rm.choices(Cafe.drinks_types, list_proba[0:6])[0]  # Extract resulting drink
            # according to these probabilities
            buyfoo = rm.choices(Cafe.food_types, list_proba[6:11])[0]  # Extract resulting food
            # according to these probabilities
            pricedrk = Cafe.menu_drinks.get(buydrk)  # Extract the price of the resulting drink
            pricefoo = Cafe.menu_food.get(buyfoo)  # Extract the price of the resulting food
            cons.append([timeslot_selected, buydrk, pricedrk, buyfoo, pricefoo])
            # Add to the output consumption list,
            # for each input timeslot, a list containing the timeslot,
            # resulting drink and its price, resulting food and its price
        return cons


# Creating a class that stores for each customer: their ID, their purchasing timeslots, their consumptions
# And can retrieve this information given an ID

class Archive:
    database_id_time = {}  # Create dictionary linking customers' ID (key) to their purchasing timeslots (value)
    database_id_cons = {}  # Create dictionary linking customers' ID (key) to their consumptions (value)

    # Creating a function that returns the dictionary value according to its key
    # to retrieve purchasing timeslots and consumptions from customers' ID if asked by the customer.
    @staticmethod
    def consult_historic(database, key):
        return database[key]

    @classmethod
    def reset(cls):
        cls.database_id_time = {}  # Reset dictionary linking customers' ID (key) to their purchasing timeslots (value)
        cls.database_id_cons = {}  # Reset dictionary linking customers' ID (key) to their consumptions (value)


# Creating a function that given a dictionary, a key and value: it checks whether the key is already
# registered in the dictionary.
# If not, the key is added with its value and the value is returned.
# Otherwise, the value is the new definition of the key in the dictionary and the difference of length
# between this new definition and the old definition is returned

def check_key(database, key, value):
    if key not in database:
        database[key] = value
        return database[key]
    else:
        additional_value = len(value) - len(database[key])
        database[key] = value
        return additional_value


# Creating a function that takes a key and a value. It returns the consumption historic linked to this key


def adding(key, value):
    if key not in Archive.database_id_time:  # If not already registered in dictionary linking ID to Timeslots
        Archive.database_id_cons[key] = Cafe.consumption(check_key(Archive.database_id_time, key, value))  # Compute
        # the consumption and register it in dictionary linking ID to consumptions
        return Archive.database_id_cons[key]  # Return consumption historic
    else:  # If already registered in dictionary linking ID to Timeslots
        Archive.database_id_cons[key] = Cafe.consumption(value[-check_key(Archive.database_id_time, key, value):]) + \
                                        Archive.database_id_cons[key]  # Compute the additional consumption and add it
        # to the consumption historic
        return Archive.database_id_cons[key]  # Return consumption historic


# Define the class Customer. For each customer registered by an ID with purchasing timeslots
# (we assumed customers are able to tell their purchasing timeslots), its purpose is to immediately store
# this customer's ID to the existing customer database if new, store this customer's purchasing timeslots
# to the existing customer database if new, and store this customer's consumption to the existing database
# if new.


class Customer(object):
    def __init__(self, ID, purchasinglist):
        self.ID = ID
        self.purchasinglist = purchasinglist
        self.ind_cons = adding(self.ID, self.purchasinglist)

    customer_budgets = {}

    # Creating a function that defines customers' initial budget and stores it in customer_budgets  dictionary
    @classmethod
    def budget(cls, x):
        cls.customer_budgets = x


# Define Once subclass of Customer. It corresponds to customers coming only once.


class Once(Customer):
    def budget_remain(self):  # Creating a function that returns customer's current budget
        budget = Customer.customer_budgets['Once']  # Initialize its budget
        for i in range(0, len(self.ind_cons)):  # Extract from each consumption drink & food prices
            budget = budget - self.ind_cons[i][2] - self.ind_cons[i][4]
        return budget


# Define Trip subclass of Once. It corresponds to customers coming only once thanks to trip advisor

class Trip(Once):
    def budget_remain(self):  # Creating a function that returns customer's current budget
        budget = Customer.customer_budgets['Trip']  # Initialize its budget
        for i in range(0, len(self.ind_cons)):  # Extract from each consumption drink & food prices
            tip = rm.randrange(1, 10)  # Define a random tip between 1 and 10 for each consumption
            budget = budget - self.ind_cons[i][2] - self.ind_cons[i][4] - tip
        return budget


# Define Return subclass of Customer. It corresponds to returning customers


class Return(Customer):
    def budget_remain(self):  # Creating a function that returns customer's current budget
        budget = Customer.customer_budgets['Return']  # Initialize its budget
        for i in range(0, len(self.ind_cons)):  # Extract from each consumption drink & food prices
            budget = budget - self.ind_cons[i][2] - self.ind_cons[i][4]
        return budget


# Define Hipster subclass of Return. It corresponds to specific returning customers: hipsters

class Hipster(Return):
    def budget_remain(self):  # Creating function that returns customer's current budget
        budget = Customer.customer_budgets['Hipster']  # Initialize its budget
        for i in range(0, len(self.ind_cons)):  # Extract from each consumption drink & food prices
            budget = budget - self.ind_cons[i][2] - self.ind_cons[i][4]
        return budget
