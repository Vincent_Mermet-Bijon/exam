import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
from Code.Customer_Classes_init import df, probamatrix  # Import data & probability matrix
# from Customer_Classes_init file

## Preparatory work ##

print(df.head(10))
df.info()
df.describe()  # General information: Number of total sales of drinks/food
# different types of sold drinks/food
print(df.dtypes)  # Check the data types: object = string in pandas

## Question 1 ##

print(df['DRINKS'].value_counts())  # Drinks sold
print(df['FOOD'].value_counts())  # Food sold

print(df['DRINKS'].nunique())  # Check: number of unique values
print(df['FOOD'].nunique())  # Check: number of unique values

print(len(df))  # Check: number of total sales
print(df['CUSTOMER'].nunique())  # Check: number of unique customers

## Question 2 ##

# Number of drinks sold by type
plt.subplot(1, 2, 1)
df['DRINKS'].value_counts().plot(kind="bar",
                                 title="Number of Drinks Sold by Type 2016-2020",
                                 figsize=(10, 5), color=['#034694', '#001C58', '#5CBFEB', '#D00027',
                                                        '#EF0107', '#DA020E'])
plt.xlabel("Drinks")
plt.ylabel("Total Sales")
drinkcolours = ['#034694', '#001C58', '#5CBFEB', '#D00027',
                '#EF0107', '#DA020E']



# Number of food sold by type
plt.subplot(1, 2, 2)

df['FOOD'].value_counts().plot(kind="bar",
                               title="Number of Food Items Sold by Type 2016-2020",
                               figsize=(10, 5), color=['#034694', '#001C58', '#5CBFEB', '#D00027'])
plt.xlabel("Foods")
plt.ylabel("Total Sales")
plt.tight_layout()
plt.show()

# Coffee consumption throughout the day
probamatrix['coffee'].plot(title='Coffee probability consumption throughout the day', figsize=(10, 5), color='C2')
plt.xlabel('Coffee probability consumption')
plt.ylabel('Time')
plt.tight_layout()
plt.show()

## Question 3


print(probamatrix)  # Display each purchasing probability for every unique fixed purchasing timeslots


# Create a function that displays the probability that a customer consumes
# a specific type of drinks or food at any given time.

def averageatanytime():
    data = probamatrix  # we use the constructed probability matrix
    timeslots = data.index.tolist()  # Extract hour and minutes of every fixed purchasing timeslots
    data_trans = data.transpose()  # transpose the matrix
    # Display for each given time the probabilities into a sentence.
    for i in range(0, len(timeslots)):
        list_proba = data_trans[timeslots[i]].tolist()  # Convert the transposed columns into a list of probabilities
        print("On average the probability for a customer at %s buying coffee is %s, "
              "frappucino is %s, milkshake is %s, soda is %s, tea is %s, water is %s. "
              "And for food: %s for cookie, %s for muffin, %s for pie, %s for sandwich and nothing is %s."
              % (timeslots[i], str(round(int(list_proba[0]), 2)) + "%", str(round(int(list_proba[1]), 2)) + "%",
                 str(round(int(list_proba[2]), 2)) + "%", str(round(int(list_proba[3]), 2)) + "%",
                 str(round(int(list_proba[4]), 2)) + "%", str(round(int(list_proba[5]), 2)) + "%",
                 str(round(int(list_proba[6]), 2)) + "%", str(round(int(list_proba[7]), 2)) + "%",
                 str(round(int(list_proba[8]), 2)) + "%", str(round(int(list_proba[9]), 2)) + "%",
                 str(round(int(list_proba[10]), 2)) + "%"))


averageatanytime()


