import random as rm
import pandas as pd
import numpy as np
from Code.Customer_Classes import Return, Hipster, Once, Trip, Cafe, Customer, Archive


# Creating 1000 return IDs with 667 regular returning customers and 333 Hipsters
# timeslots as value

class Clientele:
    set_once = set()  # Initializing a set that will contain every unique ID of one-timers
    list_return_ID = []  # Initialize the list of the returning customers
    dict_return_id_time = {}  # Initialize the dictionary of theses customers with ID as key and purchasing

    @classmethod
    def generate_returning(cls, number_returning):
        from math import ceil
        limit = ceil((2 / 3) * number_returning)
        for index in range(0, limit):
            return_id = 'RReg' + str(index)  # Store the fact that the returning customer is regular
            cls.dict_return_id_time[return_id] = []  # Initialize their purchasing timeslots
            cls.list_return_ID.append(return_id)  # Fill the list of 1000 returning with unique IDs of regulars
        for index in range(limit, number_returning):
            return_id = 'RHip' + str(index)  # Store the fact that the returning customer is an hipster
            cls.dict_return_id_time[return_id] = []  # Initialize their purchasing timeslots
            cls.list_return_ID.append(return_id)  # Fill the list of 1000 returning with unique IDs of hipsters

    @classmethod
    def reset(cls):
        cls.set_once = set()  # Reset the set that will contain every unique ID of one-timers
        cls.list_return_ID = []  # Reset the list of the returning customers
        cls.dict_return_id_time = {}  # Reset the dictionary of theses customers with ID as key and purchasing


# Create a function that returns a customer type according to its distribution in the customer population

def customer_type():
    population_customer = ['Return', 'Hipster', 'Once', 'Trip']
    weight_customer = [(2 / 3) * (1 / 5), (1 / 3) * (1 / 5), (9 / 10) * (4 / 5), (1 / 10) * (4 / 5)]
    random_customer_type = rm.choices(population_customer, weight_customer)
    return random_customer_type[0]


# Creating a function that takes a type of customer, and returns a new ID if he is a one-timer,
# a random ID from the list of the 1000 returning customers otherwise

def customer_id(type_customer):
    if type_customer == 'Return' or type_customer == 'Hipster':
        returning_id = rm.choice(Clientele.list_return_ID)
        return returning_id
    else:
        once_id = "OneTimer" + str(len(Clientele.set_once))
        Clientele.set_once.add(once_id)
        return once_id


# Creating a simulation function that simulate a dataset

def simulation(data, drinks_menu, food_menu, budget_customer, number_loyal,
               reset_archive, reset_Clientele):
    # If we need to reset the previous Clientele database and Coffee shop archives
    if reset_archive == 1:
        Archive.reset()
    if reset_Clientele == 1:
        Clientele.reset()
        Clientele.generate_returning(number_loyal)  # Create the list of loyal customers
    # Initialize Coffee shop menu, customers' budget
    Cafe.menu(drinks_menu, food_menu)
    Customer.budget(budget_customer)

    # Create an empty dataframe with the same structure of the original one
    data = pd.DataFrame().reindex_like(data)
    # Initialize its future columns
    col_simulation_customer = []
    col_simulation_drinks = []
    col_simulation_food = []

    # Fill these columns
    for index in range(0, len(data.index)):  # Fill for each 'row' (index in this case) of the dataset
        timeslot_selected = [data.index[index].strftime('%H:%M')]  # Extract the corresponding 'row' (index)
        my_type = customer_type()  # Generate the type of the customer
        my_id = customer_id(my_type)  # Generate its id (retrieve if not new)
        col_simulation_customer.append(my_id)  # Fill the customer column with its id

        if my_id[0:4] == 'RReg':  # check whether the customer is Regular Return type
            old_buy_timeslots = Clientele.dict_return_id_time.get(my_id)  # Get customer previous purchasing timeslots
            current_buy_timeslots = old_buy_timeslots + timeslot_selected  # Define customer current
            # purchasing timeslots
            Clientele.dict_return_id_time[my_id] = current_buy_timeslots  # Update timeslots in customer dictionary
            my_customer = Return(my_id, current_buy_timeslots)  # Define customer Return class
            col_simulation_drinks.append(my_customer.ind_cons[len(current_buy_timeslots) - 1][1])  # Fill the
            # drinks column with the type of drinks consumed retrieve by .ind_cons method
            col_simulation_food.append(my_customer.ind_cons[len(current_buy_timeslots) - 1][3])  # Fill the
            # food column with the type of food consumed retrieve by .ind_cons method
            if my_customer.budget_remain() < Cafe.most_expensive_menu:  # Budget Constraint
                Clientele.list_return_ID.remove(my_id)

        elif my_id[0:4] == 'RHip':  # check whether the customer is Hipster Return type
            old_buy_timeslots = Clientele.dict_return_id_time.get(my_id)  # Get customer previous purchasing timeslots
            current_buy_timeslots = old_buy_timeslots + timeslot_selected  # Define customer current
            # purchasing timeslots
            Clientele.dict_return_id_time[my_id] = current_buy_timeslots  # Update timeslots in customer dictionary
            my_customer = Hipster(my_id, current_buy_timeslots)  # Define customer Hipster class
            col_simulation_drinks.append(my_customer.ind_cons[len(current_buy_timeslots) - 1][1])  # Fill the
            # drinks column with the type of drinks consumed retrieve by .ind_cons method
            col_simulation_food.append(my_customer.ind_cons[len(current_buy_timeslots) - 1][3])  # Fill the
            # food column with the type of food consumed retrieve by .ind_cons method
            if my_customer.budget_remain() < Cafe.most_expensive_menu:  # Budget Constraint
                Clientele.list_return_ID.remove(my_id)

        elif my_type == 'Once':  # check whether the customer is Regular Once type
            my_customer = Once(my_id, timeslot_selected)  # Define customer Once class
            col_simulation_drinks.append(my_customer.ind_cons[0][1])  # Fill the
            # drinks column with the type of drinks consumed retrieve by .ind_cons method
            col_simulation_food.append(my_customer.ind_cons[0][3])  # Fill the
            # food column with the type of food consumed retrieve by .ind_cons method

        else:  # when the customer is Trip Advisor Once type
            my_customer = Trip(my_id, timeslot_selected)  # Define customer Trip class
            col_simulation_drinks.append(my_customer.ind_cons[0][1])  # Fill the
            # drinks column with the type of drinks consumed retrieve by .ind_cons method
            col_simulation_food.append(my_customer.ind_cons[0][3])  # Fill the
            # food column with the type of food consumed retrieve by .ind_cons method

    # Fill the empty structured dataset by column
    data['CUSTOMER'] = col_simulation_customer
    data['DRINKS'] = col_simulation_drinks
    data['FOOD'] = col_simulation_food
    return data


# Create a profit function that given a coffee shop dataset, and its menu,
# returns in a list the average daily revenue per consumption, daily revenue and average daily revenue

def profit(data, menu_drinks, menu_food):
    data = data.copy()
    data['FOOD'] = data['FOOD'].replace(np.nan, 'nothing')  # Replace the missing values
    data['DRINKS'].replace(menu_drinks, inplace=True)  # Replace each drinks type by its price
    data['FOOD'].replace(menu_food, inplace=True)  # Replace each food type by its price
    data['revenue'] = data['DRINKS'] + data['FOOD']  # Compute the revenue for each consumption

    # Compute the average daily revenue per consumption, daily revenue, and average daily revenue
    average_daily_revenue_per_consumption = data.groupby(data.index.strftime('%d/%m/%Y'))['revenue'].agg('mean')
    daily_revenue = data.groupby(data.index.strftime('%d/%m/%Y'))['revenue'].agg('sum')
    average_daily_revenue = sum(daily_revenue.tolist()) / len(daily_revenue.tolist())

    return [average_daily_revenue_per_consumption, daily_revenue, average_daily_revenue]
