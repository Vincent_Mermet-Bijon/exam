import matplotlib.pyplot as plt
import random as rd
import pandas as pd
from Code.Customer_Classes_init import df, display_probamatrix
from Code.Simulation_init import simulation, Clientele, profit
from Code.Customer_Classes import Archive

# Question 1: Show some buying histories of returning customers for your simulation


customer_budget = {'Return': 250, 'Hipster': 500, 'Once': 100, 'Trip': 100}
menu_drinks = {'coffee': 3, 'frappucino': 4, 'milkshake': 5, 'soda': 3, 'tea': 3, 'water': 2}
menu_food = {'cookie': 2, 'muffin': 3, 'nothing': 0, 'pie': 3, 'sandwich': 2}
number_returning = 1000

df_simulation1 = simulation(df, menu_drinks, menu_food, customer_budget, number_returning,
                            reset_archive=1, reset_Clientele=1)

# Examples: Random Buying histories of returning customer
print(Archive.database_id_cons[rd.choice(list(Clientele.dict_return_id_time))])
print(Archive.database_id_cons[rd.choice(list(Clientele.dict_return_id_time))])
print(Archive.database_id_cons[rd.choice(list(Clientele.dict_return_id_time))])


# Question 2

# How many actual returning customers in original df?


def count_frequency(my_list):  # Creating a function that count the frequency of elements in a given list
    # and returns the result through a dictionary
    my_dict = {}
    for index in my_list:
        if index in my_dict:
            my_dict[index] = 1 + my_dict[index]
        else:
            my_dict[index] = 1
    return my_dict


data = df.copy()  # Store the initial dataset
freq_customer = count_frequency(data['CUSTOMER'])  # Apply the function to the customer list
freq_return = {key: val for key, val in freq_customer.items() if val != 1}  # Extract only the returning customers

number_returning = len(freq_return)  # Number of returning customers
print(number_returning)  # 1000 actual returning customers

# Do they have specific times when they show up more?
# Can you determine a probability of having a onetime or returning customer at a given time?


binary_returning = []  # Initialize the list storing the fact whether a customer is a one-timer or not
for i in range(0, len(data['CUSTOMER'])):  # Fill the list with 0 or 1 whether the customer is a one-timer or not
    if data['CUSTOMER'][i] in freq_return:
        binary_returning.append(1)
    else:
        binary_returning.append(0)

data['binary_returning'] = binary_returning  # Create a new column in the dataset of this dummy variable

proba_return = data.groupby(data.index.strftime('%H:%M')).apply(lambda x: x['binary_returning'].
                                                                value_counts(normalize=True))[1]
print(proba_return)  # We obtain the probability of having a returning customer at each timeslot

proba_return_given_time = dict(zip(proba_return.index.tolist(), proba_return.values.tolist()))  # Into dictionary
print(proba_return_given_time)  # Probability that they show up at a certain time are shown in proba_return_given_time

maxi = sorted(proba_return_given_time, key=proba_return_given_time.get, reverse=True)[:10]
print(maxi)  # The ten most-likely timeslots for returning customers are all in the afternoon between 13:00 and 18:00
mini = sorted(proba_return_given_time, key=proba_return_given_time.get, reverse=False)[:10]
print(mini)  # The ten least popular timeslots for returning customers are all in the end of the morning

# How does this impact their buying history?
# Do you see correlations between what returning customers buy and one-timers?


df_one_timer = data.copy()
df_one_timer = df_one_timer[df_one_timer['binary_returning'] == 0]  # Create a dataset of only one-timers

df_returning = data.copy()
df_returning = df_returning[df_returning['binary_returning'] == 1]  # Create a dataset of only returners

proba_one_timer = display_probamatrix(df_one_timer)  # Compute one-timers probabilities
proba_returning = display_probamatrix(df_returning)  # Compute returners probabilities
difference_once_return = proba_one_timer.subtract(proba_returning)  # Difference in probabilities

print(proba_one_timer)  # One-time customers consumption probabilities by timeslot
print(proba_returning)  # Returning customers consumption probabilities by timeslot
print(difference_once_return)  # Difference in probabilities

# Analytics

difference_once_return.plot(title='Product consumption probability difference '
                                  '(One-timers - returning) over a day')
plt.ylabel('Probability difference (%)')
plt.xlabel('Time')
plt.show()

behaviour_difference = {}
for i in range(0, len(difference_once_return.columns.tolist())):
    column_name = difference_once_return.columns.tolist()[i]
    average_difference = sum(difference_once_return[column_name]) / len(difference_once_return[column_name])
    behaviour_difference[column_name] = average_difference

print(behaviour_difference)  # Overall Correlation between returning customer and one-timers
# (Returners significantly consume more coffee and less others drinks)

# Returning customers have a much higher percentage of consuming coffee in the morning compared to one-time customers.
# One-time customers have a higher chance of consuming everything else. Then starting around 11:00 the probability
# distributions become similar and there do not seem to be major differences between consumption patterns of one-time
# and returning customers.


# Question 3: Consequences of reducing returning customers to 50 and simulating the same period

customer_budget = {'Return': 250, 'Hipster': 500, 'Once': 100, 'Trip': 100}
menu_drinks = {'coffee': 3, 'frappucino': 4, 'milkshake': 5, 'soda': 3, 'tea': 3, 'water': 2}
menu_food = {'cookie': 2, 'muffin': 3, 'nothing': 0, 'pie': 3, 'sandwich': 2}
number_returning = 50

try:
    df_simulation2 = simulation(df, menu_drinks, menu_food, customer_budget, number_returning,
                                reset_archive=1, reset_Clientele=1)
except IndexError:
    print("Returning customers have spent all budget before end of simulation. "
          "Simulation therefore stops before the end. Moving onto Question 4.")

# The simulation process stops at some point.  At this point, all returning customers' have spent
# all their budget before the end of the simulation.
# Once a returning type is randomly chosen, the pool of returning customer available is empty.
# Thus the simulation process stops.


# Question 4: The prices change from the beginning of 2018 and go up by 20%

customer_budget = {'Return': 250, 'Hipster': 500, 'Once': 100, 'Trip': 100}
menu_drinks = {'coffee': 3, 'frappucino': 4, 'milkshake': 5, 'soda': 3, 'tea': 3, 'water': 2}
menu_food = {'cookie': 2, 'muffin': 3, 'nothing': 0, 'pie': 3, 'sandwich': 2}
menu_drinks_after = {'coffee': 3.6, 'frappucino': 4.8, 'milkshake': 6, 'soda': 3.6, 'tea': 3.6, 'water': 2.4}
menu_food_after = {'cookie': 2.4, 'muffin': 3.6, 'nothing': 0, 'pie': 3.6, 'sandwich': 2.4}
number_returning = 1000

# Placebo simulation
df_simulation3_placebo = simulation(df, menu_drinks, menu_food, customer_budget, number_returning,
                                    reset_archive=1, reset_Clientele=1)
nb_remaining_placebo = len(Clientele.list_return_ID)  # Remaining available returning customers for placebo

# Inflation simulation
df_simulation3_before = simulation(df[df.index.strftime('%Y') < "2018"], menu_drinks, menu_food,
                                   customer_budget, number_returning, reset_archive=1, reset_Clientele=1)
df_simulation3_after = simulation(df[df.index.strftime('%Y') >= "2018"], menu_drinks_after, menu_food_after,
                                  customer_budget, number_returning, reset_archive=0, reset_Clientele=0)
df_simulation_inflation = pd.concat([df_simulation3_before, df_simulation3_after])  # Merge the two datasets
nb_remaining_inflation = len(Clientele.list_return_ID)  # Remaining available returning customers for inflation

# Compare both simulation
print(nb_remaining_placebo - nb_remaining_inflation)  # Compare available remaining returning customers ...
print(profit(df_simulation3_placebo, menu_drinks, menu_food)[2] -  # ... and average daily income
      0.4 * profit(df_simulation3_before, menu_drinks, menu_food)[2] -
      0.6 * profit(df_simulation3_after, menu_drinks_after, menu_food_after)[2])

# Less available remaining returning customers but more average daily income.


# Question 5: The budget of hipsters drops to 40

customer_budget = {'Return': 250, 'Hipster': 40, 'Once': 100, 'Trip': 100}
menu_drinks = {'coffee': 3, 'frappucino': 4, 'milkshake': 5, 'soda': 3, 'tea': 3, 'water': 2}
menu_food = {'cookie': 2, 'muffin': 3, 'nothing': 0, 'pie': 3, 'sandwich': 2}
number_returning = 1000

try:
    df_simulation4 = simulation(df, menu_drinks, menu_food, customer_budget, number_returning,
                                reset_archive=1, reset_Clientele=1)
except IndexError:
    print("Returning customers have spent all budget before end of simulation. "
          "Simulation therefore stops before the end. Moving onto Question 6.")

# The simulation process stops at some point.  At this point, all returning customers' have spent
# all their budget before the end of the simulation.
# Once a returning type is randomly chosen, the pool of returning customer available is empty.
# Thus the simulation process stops.


# QUESTION 6
# The prices of coffee bars are regulated by the City mayor in coordination with the coffee bar owners. During the
# city mayoral election in 2016, one candidate (he lost :( (due to massive fraud in mail-in-ballots (obviously)) wanted
# to increase coffee bar income. The mayor therefore asked the coffee bars to choose the drink for which they would
# like a 1-euro price increase to apply. Which product would the coffee bars choose if this policy were implemented.

customer_budget = {'Return': 250, 'Hipster': 500, 'Once': 100, 'Trip': 100}
menu_drinks = {'coffee': 3, 'frappucino': 4, 'milkshake': 5, 'soda': 3, 'tea': 3, 'water': 2}
menu_food = {'cookie': 2, 'muffin': 3, 'nothing': 0, 'pie': 3, 'sandwich': 2}
number_returning = 1000


# Creating a function that returns the daily average profit from each different simulation corresponding
# to each time a drink price goes up by 1 euro.

def policy_one_euro(drinks, food):
    dict_average_daily_profit = {}  # Initialise the dictionary returning the profit
    offer_food = food
    for index in range(0, len(drinks)):  # Looping for 6 different simulations for the 6 different drinks
        offer_drinks = drinks.copy()
        offer_drinks[[*offer_drinks][index]] = offer_drinks[[*offer_drinks][index]] + 1  # Adding 1$
        increase_drinks = simulation(df, offer_drinks, offer_food, customer_budget, number_returning,
                                     reset_archive=1, reset_Clientele=1)  # Run simulation
        dict_average_daily_profit[[*offer_drinks][index]] = profit(increase_drinks, offer_drinks,
                                                                   offer_food)[2]  # Fill the dictionary
    return dict_average_daily_profit


print(policy_one_euro(menu_drinks, menu_food))  # Run the analysis (duration 5 mn max)
# Coffee shop would have increased soda price
