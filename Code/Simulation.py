import numpy as np
import matplotlib.pyplot as plt
from Code.Customer_Classes_init import df
from Code.Simulation_init import simulation, profit

# Simulation process

customer_budget = {'Return': 250, 'Hipster': 500, 'Once': 100, 'Trip': 100}
menu_drinks = {'coffee': 3, 'frappucino': 4, 'milkshake': 5, 'soda': 3, 'tea': 3, 'water': 2}
menu_food = {'cookie': 2, 'muffin': 3, 'nothing': 0, 'pie': 3, 'sandwich': 2}
number_returning = 1000

df_simulation = simulation(df, menu_drinks, menu_food, customer_budget, number_returning,
                           reset_archive=1, reset_Clientele=1)

df_simulation.loc[df_simulation['FOOD'] == 'nothing', 'FOOD'] = np.nan  # Put missing values instead of 'nothing'

print(df_simulation.head(10))  # Simulation visualisation


# Comparison with graphs in 1.2

colors = ['#034694', '#001C58', '#5CBFEB', '#D00027', '#EF0107', '#DA020E']

# Graph 1

plt.subplot(1, 2, 1)
df['DRINKS'].value_counts().plot(kind="bar",
                                 title="Total Drinks Sold - Real Data",
                                 figsize=(10, 5), color=colors)
plt.xlabel("Drinks")
plt.ylabel("Total Sales")

plt.subplot(1, 2, 2)
df_simulation['DRINKS'].value_counts().plot(kind="bar",
                                            title="Total Drinks Sold - Simulation Data",
                                            figsize=(10, 5), color=colors)
plt.xlabel("Drinks")
plt.ylabel("Total Sales")
plt.tight_layout()
plt.show()

# Graph 2

plt.subplot(1, 2, 1)
df['FOOD'].value_counts().plot(kind="bar",
                               title="Total Food Items Sold - Real Data",
                               figsize=(10, 5), color=colors)
plt.xlabel("Food")
plt.ylabel("Total Sales")

plt.subplot(1, 2, 2)
df_simulation['FOOD'].value_counts().plot(kind="bar",
                                          title="Total Food Items Sold - Simulation Data",
                                          figsize=(10, 5), color=colors)
plt.xlabel("Food")
plt.ylabel("Total Sales")
plt.tight_layout()
plt.show()


# Compute the average daily revenue

# Compute the average daily revenue for initial dataset
df_profit = profit(df, menu_drinks, menu_food)

# Compute the average daily revenue for simulation
df_simulation_profit = profit(df_simulation, menu_drinks, menu_food)


# Plot

profits_real = df_profit[1]  # Real daily revenue
profits_sim = df_simulation_profit[1]  # Simulated daily revenue

plt.subplot(2, 1, 1)
profits_real.plot(kind="line",
                  title="Daily Profits - Real Data",
                  figsize=(10, 5))
plt.xlabel("")
plt.ylabel("Profits")
plt.xticks([])

plt.subplot(2, 1, 2)
profits_sim.plot(kind="line",
                 title="Daily Profits - Simulated Data",
                 figsize=(10, 5))
plt.xlabel("Date")
plt.ylabel("Profits")
plt.show()
